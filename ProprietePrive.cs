namespace pacmanascii
{
    class ProprietePrive
    {
        private int Champspublic {get;set;}
        public int _Champspublic {
            get { return this.Champspublic;}
            set { this.Champspublic = value;}
        }
    }

    class TestProprietePrive : AbstractTest
    {
        public TestProprietePrive(string name) : base(name){}

        override public bool test()
        {
            ProprietePrive ppv = new ProprietePrive();
            ppv._Champspublic = 42;
            if (ppv._Champspublic == 42)
            {
                return true; 
            }
            else
            {
                return false;
            }
        }
    }
}