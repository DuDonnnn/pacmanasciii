﻿using System;

namespace pacmanascii
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string s in args)
            {
                switch (s)
                {
                    case "testchampspublic":
                        TestChampsPublic tcp = new TestChampsPublic("Test Champs Public");
                        tcp.Main(new string [0]);
                        break;
                    case "testproppublic":
                        TestProprietePublic tpp = new TestProprietePublic("Test Propriete Public");
                        tpp.Main(new string [0]);
                        break;
                    case "testpropprive":
                        TestProprietePrive tppv = new TestProprietePrive("Test Propriete Prive");
                        tppv.Main(new string [0]);
                        break;
                    case "testchampsprive":
                        TestChampsPrive tcpv = new TestChampsPrive("Test Champs Prive");
                        tcpv.Main(new string [0]);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
