using System;

 namespace pacmanascii
 {
    abstract class AbstractTest : ITest
    {
        public AbstractTest(string name) {this.Name  = name;}
    
        public string Name {get; private set;}

        virtual protected void WriteOK() 
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" [");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("OK");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("] : ");
        }

        virtual protected void WriteKO() 
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" [");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("KO");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("] : ");
        }

        virtual public void Main(string[]args)
        {
            if(test()){
                WriteOK();
                Console.WriteLine(this.Name);
            }
            else{
                WriteKO();
                Console.WriteLine(this.Name);
            }
        }

        abstract public bool test();
    }
 }